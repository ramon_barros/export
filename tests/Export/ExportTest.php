<?php

use Export\AbstractTest as AbstractTest;
use Export\Export;

class ExportTest extends AbstractTest {
    public $instance;

    /**
     * Antes de cada teste verifica se a classe existe
     * e cria uma instancia da mesma
     * @return void
     */
    public function assertPreConditions()
    {   
        $this->assertTrue(
                class_exists($class = 'Export\Export'),
                'Class not found: '.$class
        );
        $this->instance = new Export();
    }

    public function testInstantiationWithoutArgumentsShouldWork(){
        $this->assertInstanceOf('Export\Export', $this->instance);
    }

    /**
     * @expectedException Exception
     * @expectedExceptionMessage Você deve informar um inteiro ou decimal.
     */
    public function testSetStrWithInvalidDataShouldWork()
    {
        $instance = new Export();
    }

    /**
     * @depends testInstantiationWithoutArgumentsShouldWork
     */
    public function testSetStrWithValidDataShouldWork()
    {
        /*
        $this->assertEquals(new Export(), $this->instance, 'Returned value should be the same instance for fluent interface');
        
        $integer = 1;
        $instance = new Fraction($integer);
        $this->assertAttributeEquals("1", 'str', $instance, 'Attribute was not correctly set');
        $this->assertAttributeEquals(null, 'c_virgula', $instance, 'Attribute was not correctly set');
        $this->assertAttributeEquals("1/1", 'fraction', $instance, 'Attribute was not correctly set');
        */
    }

}