<?php
/**
 * This file is part of the Nocriz API (http://nocriz.com)
 *
 * Copyright (c) 2013  Nocriz API (http://nocriz.com)
 *
 * For the full copyright and license information, please view
 * the file license.txt that was distributed with this source code.
 */

/**
 * Nocriz API 
 *
 * @package  Nocriz
 * @author   Ramon Barros <contato@ramon-barros.com>
 */

/*
|--------------------------------------------------------------------------
| Register The Auto Loader
|--------------------------------------------------------------------------
|
| This application is installed by the Composer, 
| that provides a class loader automatically.
| Use it to seamlessly and feel free to relax.
|
*/

require __DIR__.'/../app/bootstrap.php';

use Illuminate\Database\Capsule\Manager as DB;

use Illuminate\Events\Dispatcher;
use Illuminate\Container\Container;

$db = new DB;

$db->addConnection(array(
    'driver'    => 'mysql',
    'host'      => 'localhost',
    'database'  => 'database',
    'username'  => 'root',
    'password'  => '',
    'charset'   => 'utf8',
    'collation' => 'utf8_unicode_ci',
    'prefix'    => '',
));
$db->setEventDispatcher(new Dispatcher(new Container));
$db->setAsGlobal();
// Setup the Eloquent ORM... (optional; unless you've used setEventDispatcher())
$db->bootEloquent();

/**
 * Recupera informações do banco de dados
 * @var array
 */
$users = DB::select('SELECT * FROM `users` WHERE group_id = 4');

$activities = array();

/**
 * Cria um array com o id do usuário contendo as perguntas e suas respostas
 */
foreach ($users as $user) {

    $id = $user['id'];

    if (!isset($activities[$id])) {
        $activities[$id] = array();
        $activities[$id]['name'] = $user['name'];
        $activities[$id]['email'] = $user['email'];
        $activities[$id]['points'] = $user['points'];
    }

    $results = DB::select('SELECT activities.id, activities.name AS activitie, answers.name AS answer, user_answers.text, header.name AS header, user_answers.image
                            FROM `user_answers` 
                            LEFT JOIN activities ON activities.id = user_answers.activity_id
                            LEFT JOIN users ON users.id = user_answers.user_id
                            LEFT JOIN answers ON answers.id = user_answers.answer_id
                            LEFT JOIN header ON header.id = user_answers.header_id
                            WHERE `user_id` = ?', array($id));

    if (count($results) == 0) {
        $activities[$id]['activities'] = array();
    } else {
        foreach ($results as $activitie) {
            if (!isset($activities[$id]['activities'][$activitie['id']])) {
                $activities[$id]['activities'][$activitie['id']] = array();
            }
            $activities[$id]['activities'][$activitie['id']]['question'] = htmlspecialchars($activitie['activitie']);
            $activities[$id]['activities'][$activitie['id']]['answers'][] = array(
                 'answer' => $activitie['answer']
                ,'text' => $activitie['text']
                ,'header' => $activitie['header']
                ,'image' => $activitie['image']
            );
        }
    }
}

//echo "<pre>";

//var_dump($activities);

//die();

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("Ramon Barros")
                             ->setLastModifiedBy("Ramon Barros")
                             ->setTitle("Project")
                             ->setSubject("Project")
                             ->setDescription("Project")
                             ->setKeywords("office 2007 project")
                             ->setCategory("Project");

// Bold para os campos das celulas
$styleArray = array(
        'font' => array(
            'bold' => true
        )
);

$x=1;
foreach ($activities as $id => $user) {

    /**
     * Cada usuário possui diversas atividades
     * adiciona a linha informando os dados
     */
    $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("A{$x}", "Nome")
            ->setCellValue("B{$x}", "E-mail")
            ->setCellValue("C{$x}", "id")
            ->setCellValue("D{$x}", "points");

    /**
     * Aplica bolde nos textos do campos acima
     */
    $objPHPExcel->getActiveSheet()->getStyle("A{$x}")->applyFromArray($styleArray);
    $objPHPExcel->getActiveSheet()->getStyle("B{$x}")->applyFromArray($styleArray);
    $objPHPExcel->getActiveSheet()->getStyle("C{$x}")->applyFromArray($styleArray);
    $objPHPExcel->getActiveSheet()->getStyle("D{$x}")->applyFromArray($styleArray);

    /**
     * Próxima linha
     */
    $x++;

    /**
     * Adiciona as informações do usuário
     */
    $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("A{$x}", $user['name'])
            ->setCellValue("B{$x}", $user['email'])
            ->setCellValue("C{$x}", $id)
            ->setCellValue("D{$x}", $user['points']);

    /**
     * Próxima linha.
     */
    $x++;

    /**
     * Verifica se o usuário possui atividades
     */
    if (count($user['activities']) == 0) {
        /**
         * Adiciona linha com texto "Sem respostas." em negrito
         */
        $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue("A{$x}", 'Sem respostas.')
                    ->setCellValue("B{$x}", '')
                    ->setCellValue("C{$x}", '')
                    ->setCellValue("D{$x}", '');
        $objPHPExcel->getActiveSheet()->getStyle("A{$x}")->applyFromArray($styleArray);

        /**
         * Pula 2 linhas
         * @var integer
         */
        $x = $x + 2;
    } else {

        /**
         * Adiciona os campos informando o conteúdo dos dados
         */
        $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue("A{$x}", 'Atividade')
                    ->setCellValue("B{$x}", 'Resposta')
                    ->setCellValue("C{$x}", 'Resposta Complementar')
                    ->setCellValue("D{$x}", '');
        /**
         * Adiciona bold nos campos acima
         */
        $objPHPExcel->getActiveSheet()->getStyle("A{$x}")->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->getStyle("B{$x}")->applyFromArray($styleArray);
        $objPHPExcel->getActiveSheet()->getStyle("C{$x}")->applyFromArray($styleArray);

        /**
         * Proxima linha
         */
        $x++;

        /**
         * Atividades do usuário
         * @var array
         */
        foreach ($user['activities'] as $idactivitie => $answers) {
            
            /**
             * Contandor de respostas
             * @var integer
             */
            $y=1;
            foreach ($answers['answers'] as $answer) {

                $image = false;
                
                $b = !is_null($answer['header']) ? $answer['header'] : $answer['answer'];
                $c = !is_null($answer['header']) ? $answer['answer'] : $answer['text'];

                if (is_null($b) && is_null($c) && !is_null($answer['image'])) {
                    $b = htmlspecialchars($answer['image']);
                    $image = true;
                }

                /**
                 * Se for a primeira resposta adiciona a pergunta na coluna A{x}
                 * @var integer
                 */
                if ($y == 1) {
                    $objPHPExcel->setActiveSheetIndex(0)
                                ->setCellValue("A{$x}", $answers['question'])
                                ->setCellValue("B{$x}", $b)
                                ->setCellValue("C{$x}", $c)
                                ->setCellValue("D{$x}", '');
                    /**
                     * Se for uma imagem cria um link
                     */
                    if ($image) {
                        $objPHPExcel->getActiveSheet()->getCell("B{$x}")->getHyperlink()->setUrl('http://link/files/user_answer/image/'.$id.'/' . urlencode($b));
                    }
                    /** Proxima linha **/
                    $x++;
                }
                /**
                 * Se não for a primeira resposta deixa a coluna A{x} vazia e
                 * adiciona as respostas
                 */
                if ($y > 1) {
                    $objPHPExcel->setActiveSheetIndex(0)
                                ->setCellValue("A{$x}", '')
                                ->setCellValue("B{$x}", $b)
                                ->setCellValue("C{$x}", $c)
                                ->setCellValue("D{$x}", '');
                    /**
                     * Se for uma imagem cria um link
                     */
                    if ($image) {
                        $objPHPExcel->getActiveSheet()->getCell("B{$x}")->getHyperlink()->setUrl('http://link/files/user_answer/image/'.$id.'/' . urlencode($b));
                    }

                    /** proxima linha **/
                    $x++;
                }

                /**
                 * Proxima resposta
                 */
                $y++;
            }
        }

        /**
         * Proxima linha
         */
        $x++;

        /**
         * Adiciona uma linha vazia apos o final das respostas dos usuários
         */
        $objPHPExcel->setActiveSheetIndex(0)
                                ->setCellValue("A{$x}", '')
                                ->setCellValue("B{$x}", '')
                                ->setCellValue("C{$x}", '')
                                ->setCellValue("D{$x}", '');
    }
}

//var_dump($objPHPExcel);

//die();

/**
 * Configurações nas celulas
 * redimenciona automaticamente a coluna A
 * redimenciona automaticamente a coluna B
 * redimenciona a coluna C para 50
 */
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(50);

// Rename worksheet
$objPHPExcel->getActiveSheet()->setTitle('activities');

// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);

// Redirect output to a client’s web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="activities.xls"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;