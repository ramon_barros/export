<?php namespace Export;
/**
 * This file is part of the Nocriz API (http://nocriz.com)
 *
 * Copyright (c) 2013  Nocriz API (http://nocriz.com)
 *
 * For the full copyright and license information, please view
 * the file license.txt that was distributed with this source code.
 */

/**
 * Fraction Library convert decimal to fraction.
 *
 * @package  Math
 * @author   Ramon Barros <contato@ramon-barros.com>
 */

class Export {

    private $version = '1.0';

}