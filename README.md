# Export

### Export Database para arquivo

Você pode instalar com Composer (recomendado) ou manualmente.

```
$ curl -sS https://getcomposer.org/installer | php
$ php composer.phar install --prefer-source
```

### Tests

Tests sem Coverage
```
$ bin/phpunit --configuration phpunit.xml
```

Tests com coverage
```
# Requer extensão Xdebug.
$ bin/phpunit --configuration phpunit.xml.dist
```